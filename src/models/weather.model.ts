export class weatherModel{ 
        coord: coord
        weather: weatherview
        base: string
        main: main
        visibility: number
        wind: wind
        clouds: clouds
        dt: number
        sys: sys
        timezone: number
        id: number
        name: string
        cod: number
        date: Date
        constructor(){
            this.coord=new coord
            this.weather=new weatherview
            this.base="";
            this.main=new main;
            this.visibility=0;
            this.wind=new wind;
            this.clouds=new clouds;
            this.dt=0;
            this.sys=new sys;
            this.timezone=0;
            this.id=0;
            this.name="";
            this.cod=0;
            this.date=new Date();
        }
        
}


export class coord{
    lon: number
    lat: number
    constructor(){
        this.lon=0
        this.lat=0
    }
}

export class weatherview{
    id: number
    main: string
    description: string
    icon: string
    constructor(){
        this.id=0;
        this.main="";
        this.description="";
        this.icon="";
    }
}


export class main{
    
        temp: number
        feels_like: number
        temp_min: number
        temp_max: number
        pressure: number
        humidity: number
        sea_level: number
        grnd_level: number
        constructor(){
            this.temp=0
            this.feels_like=0
            this.temp_min=0
            this.temp_max=0
            this.pressure=0
            this.humidity=0
            this.sea_level=0
            this.grnd_level=0
        }
}


export class wind{
    speed: number
    deg: number 
    gust: number
    constructor(){
        this.speed=0
        this.deg=0
        this.gust=0
    }
    
}

export class clouds{
    alt: number
    constructor(){
        this.alt=0
    } 
}


export class sys{
    country: string
    sunrise: number
    sunset: number
    constructor(){
        this.country=""
        this.sunrise=0
        this.sunset=0
    }

    

}

/*
{
    "coord": {
        "lon": -65.4117,
        "lat": -24.7859
    },
    "weather": [
        {
            "id": 803,
            "main": "Clouds",
            "description": "broken clouds",
            "icon": "04d"
        }
    ],
    "base": "stations",
    "main": {
        "temp": 301.99,
        "feels_like": 301.87,
        "temp_min": 301.99,
        "temp_max": 301.99,
        "pressure": 1014,
        "humidity": 43,
        "sea_level": 1014,
        "grnd_level": 889
    },
    "visibility": 10000,
    "wind": {
        "speed": 1.83,
        "deg": 91,
        "gust": 3.41
    },
    "clouds": {
        "all": 68
    },
    "dt": 1674137443,
    "sys": {
        "country": "AR",
        "sunrise": 1674121720,
        "sunset": 1674170132
    },
    "timezone": -10800,
    "id": 3838233,
    "name": "Salta",
    "cod": 200
}
*/