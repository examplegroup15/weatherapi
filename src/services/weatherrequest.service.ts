import { Component, OnInit } from '@angular/core';
import { weatherModel } from "src/models/weather.model";
import {Injectable} from '@angular/core';
import { HttpClient} from  '@angular/common/http'

@Injectable({
    providedIn:'root'
})

export class weatherrequestservice{
    constructor(
        private http: HttpClient){} 
    getweather(ciudad: string){
        return this.http.get<weatherModel>("https://api.openweathermap.org/data/2.5/weather?q="+ciudad+"&appid=b8aa3b2278774ff38b89027f584b1f74&units=metric");
    }
}