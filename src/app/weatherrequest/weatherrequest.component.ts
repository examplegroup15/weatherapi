import { Component, OnInit, ViewChild } from '@angular/core';
import { weatherModel } from 'src/models/weather.model';
import {weatherrequestservice } from 'src/services/weatherrequest.service';
import {MatTable} from '@angular/material/table';
import { __values } from 'tslib';

import { NgxSpinnerService } from "ngx-spinner";


interface city{
  name: string;
}

@Component({
  selector: 'app-weatherrequest',
  templateUrl: './weatherrequest.component.html',
  styleUrls: ['./weatherrequest.component.css']
})




export class WeatherrequestComponent implements OnInit {
  
   //especificacion de los nombres de las columnas 
  cities: city[]=[
    {name: "San juan"},
    {name:"Salta"},
    {name:"Buenos aires"},
    {name:"Cordoba"}
  ]; //array donde guardaremos la respuesta del servidor 
  dataSource: any[]=[]; //datasource de la tabla 
  displayedColummnss: string[] =["Pais","ciudad","clima","sensacion termica","fecha"] //id de columnas de la tabla
  selected = '';  //elemento que cambiaria con el selector en el html, sirve para hacer las consultas
  checkvalue: boolean=false;  //variable que tendra el valor del checkbox 
  localstore: any[]=[]
  show: boolean=false;
  original: boolean=false;
  disabled=true;
  //especificacion de clases para actualizar datos
  @ViewChild(MatTable) table!: MatTable<any>;

  constructor(public weatherservice: weatherrequestservice,private spinner: NgxSpinnerService){}

 
  ngOnInit(): void { }
  

  

  checkCheckBoxvalue(event: { checked: any; }){
    this.checkvalue=event.checked;
  }

  citychanged(){//que no se muestren las consultas anteriores al tocar el select 
    this.show=false;
    this.disabled=false;
    this.original=false;
  }


  //cuando aprieten el boton se mostrara la informacion 
  buttonpressed(){
    this.original=false;
    this.spinner.show();
    

      
      if(!this.checkvalue){   //si NO esta chequeado el checkbox 
        this.dataSource=[];
        this.weatherservice.getweather(this.selected).subscribe((res: weatherModel) => {
          this.show=false;
          this.dataSource.push(res)
          this.original=true;
          this.spinner.hide();  //el spinner tiene que ir en eventos asincronicos ¡???
          

          /*this.table.renderRows();  */
        })
        
      }

      else{ //caso contrario , checkbox chekeado
        const datetime=new Date();
        console.log(datetime) //probando la fecha 
        this.original=false;
        this.show=true;

        this.dataSource=[];
        
        this.weatherservice.getweather(this.selected).subscribe((res: weatherModel) => {
            res.date=datetime;//hacemos que la respuesta de la api tenga la fecha antes de que la guardemos
            this.localstore=JSON.parse(localStorage.getItem(res.name) || "[]"); //linea que trae como array del local storage los elementos que coincidan con el nombre de la ciudad
            this.dataSource=this.dataSource.concat(this.localstore) // concatena el datastore los elementos de localstore
            this.dataSource.unshift(res)//se agrega en la primera posicion el elemento que sacamos de la api
            localStorage.setItem(res.name,JSON.stringify(this.dataSource))
            this.table.renderRows();
            this.original=true;
            this.spinner.hide();
        //el set item es localStorage.setItem(this.selected.province,JSON.stringify(this.dataSource));

        //this.DataSource=JSON.parse(localStorage.getItem("res.name")); //esta linea deberia igualar el datasource al elemento del local storage ,el del local storage deberia ser de tipo array

        }) 
        
      }
      
  }
  
    
}





