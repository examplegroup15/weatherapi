import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WeatherrequestComponent } from './weatherrequest.component';

describe('WeatherrequestComponent', () => {
  let component: WeatherrequestComponent;
  let fixture: ComponentFixture<WeatherrequestComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WeatherrequestComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(WeatherrequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
